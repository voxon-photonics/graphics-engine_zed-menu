#pragma once

#include "frame.h"
#include "./input/IBindable.h"

class Scene {
public:

	Scene(std::string scene_name = "default_scene");

	void AddFrame(const std::string frame_name, int frame_layer);
	void AddFrame(const std::string json_path);

	std::shared_ptr<FrameLL> GetFrameList();
	std::shared_ptr<Frame> GetActiveFrame();

	void NextActiveFrame();
	void LastActiveFrame();

	void ToggleAciveFrameVisible();

	int GetFrameCount();

	void LoadElement();
	void NextElement();
	void LastElement();

	void MoveElementBackward();
	void MoveElementForward();
	void MoveElementLeft();
	void MoveElementRight();

	void RenameFrame(std::string new_name);

	void NewFrame();
	void SaveFrame();
	void LoadFrame();
	void ToggleFrameVisible();
	void ToggleFrameFlattened();

	void MoveFrameUp();
	void MoveFrameDown();

	void SaveScene();
	json ToJson();

	void Draw();
	void Report(int x, int y, int fcol, int bcol);

	std::shared_ptr<Bindable> GetBindings();

private:
	std::shared_ptr<FrameLL> frame_head = NULL;
	std::shared_ptr<FrameLL> active_frame = NULL;
	int updated = 0;
	int frame_count = 0;
	std::shared_ptr<Bindable> Bindings = std::make_shared<Bindable>();

	std::string scene_name;

	void BuildBindings();
};
