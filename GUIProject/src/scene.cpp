#include "scene.h"

#include "voxiebox.h"
#include "file_io.h"
#include <fstream>

Scene::Scene(std::string scene_name)
{
	this->scene_name = "default_scene";

	BuildBindings();
}

void Scene::AddFrame(const std::string frame_name, int frame_layer){
	if (frame_head == NULL) {
		frame_head = std::make_shared<FrameLL>();
		frame_head->data = std::make_shared <Frame>(frame_layer, frame_name);
		frame_head->next = NULL;
		frame_head->last = NULL;
		active_frame = frame_head;
	}
	else {
		std::shared_ptr<FrameLL> current = frame_head;
		while (current->next != NULL) {
			current = current->next;
		}

		current->next = std::make_shared<FrameLL>();
		current->next->data = std::make_shared<Frame>(frame_layer, frame_name);
		current->next->last = current;
		current->next->next = NULL;
	}

	frame_count++;
}

void Scene::AddFrame(const std::string json_path)
{
	std::ifstream json_file(json_path);
	json new_frame;
	json_file >> new_frame;

	if (frame_head == NULL) {
		frame_head = std::make_shared<FrameLL>();
		frame_head->data = std::make_shared<Frame>(new_frame);
		frame_head->next = NULL;
		frame_head->last = NULL;
		active_frame = frame_head;
	}
	else {
		std::shared_ptr<FrameLL> current = frame_head;
		while (current->next != NULL) {
			current = current->next;
		}

		current->next = std::make_shared<FrameLL>();
		current->next->data = std::make_shared<Frame>(new_frame);
		current->next->last = current;
		current->next->next = NULL;
	}

	frame_count++;
}

std::shared_ptr<FrameLL> Scene::GetFrameList()
{
	return frame_head;
}

std::shared_ptr<Frame> Scene::GetActiveFrame()
{
	if (active_frame != NULL) {
		return active_frame->data;
	}
	else {
		return NULL;
	}	
}

void Scene::NextActiveFrame()
{
	if (active_frame == NULL) active_frame = frame_head;
	else if (active_frame->next != NULL) active_frame = active_frame->next;
	else active_frame = frame_head;
}

void Scene::LastActiveFrame()
{
	if (active_frame == NULL) active_frame = frame_head;
	else if (active_frame->last != NULL) active_frame = active_frame->last;
	else while (active_frame->next) active_frame = active_frame->next;
}

void Scene::ToggleAciveFrameVisible()
{
	if (active_frame) active_frame->data->ToggleVisible();
}

int Scene::GetFrameCount()
{
	return frame_count;
}

void Scene::LoadElement()
{
	updated++;
	std::string filename = GetFileName(std::string("Choose a file"), IMAGE);
	if (filename != "" && GetActiveFrame() != NULL)
	{
		GetActiveFrame()->AddElement(filename);
	}
}

void Scene::NextElement()
{
	GetActiveFrame()->NextActiveElement();
}

void Scene::LastElement()
{
	GetActiveFrame()->LastActiveElement();
}

void Scene::MoveElementBackward()
{
	updated++;
	GetActiveFrame()->MoveActiveBackward();
}

void Scene::MoveElementForward()
{
	updated++;
	GetActiveFrame()->MoveActiveForward();
}

void Scene::MoveElementLeft()
{
	updated++;
	GetActiveFrame()->MoveActiveLeft();
}

void Scene::MoveElementRight()
{
	updated++;
	GetActiveFrame()->MoveActiveRight();
}

void Scene::RenameFrame(std::string new_name)
{
	GetActiveFrame()->SetFrameName(new_name);
}

void Scene::NewFrame()
{
	AddFrame("new frame", GetFrameCount() - 1);
	frame_count++;
}

void Scene::SaveFrame()
{
	GetActiveFrame()->SaveFrame();
}

void Scene::LoadFrame()
{
	std::string filename = GetFileName("Choose a file", JSON);
	if (filename != "")
	{
		AddFrame(filename);
	}
}

void Scene::ToggleFrameVisible()
{
	updated++;
	GetActiveFrame()->ToggleActiveVisible();
}

void Scene::ToggleFrameFlattened()
{
	updated++;
	GetActiveFrame()->ToggleShowFlattened();
}

void Scene::MoveFrameUp()
{
	GetActiveFrame()->ActiveUpList();
}

void Scene::MoveFrameDown()
{
	GetActiveFrame()->ActiveDownList();
}

void Scene::SaveScene()
{
	std::string filepath;

	const int len = 255;
	char pBuf[len];
	char drive[5];
	char path[len];

	int bytes = GetModuleFileName(NULL, pBuf, len);
	if (bytes == 0) {
		filepath = ".";
	}
	else {

		_splitpath_s((const char*)pBuf, drive, 5, path, len, NULL, 0, NULL, 0);
	}

	
	const std::string out_file_path = std::string(drive) + std::string(path) + scene_name + ".json";
	MessageBox(0, out_file_path.c_str(), "", MB_OK);

	std::ofstream out_file(out_file_path);

	out_file << ToJson();
}

json Scene::ToJson()
{
	json scene_json;

	// Scene Local Variables
	scene_json["frames"] = json();

	std::shared_ptr<FrameLL> current = frame_head;
	while (current) {
		scene_json["frames"].push_back(current->data->ToJson());
		current = current->next;
	}
	return scene_json;
}

void Scene::Draw()
{
	if (updated) {
		GetActiveFrame()->UpdateActive();
		if (GetActiveFrame()->IsShowFlattened()) GetActiveFrame()->MergeElements();

		updated = 0;
	}

	std::shared_ptr<FrameLL> current_frame = GetFrameList();

	while (current_frame != NULL) {
		if (current_frame->data->IsVisible()) {
			current_frame->data->Draw();
		}

		current_frame = current_frame->next;
	}
}

void Scene::Report(int x, int y, int fcol, int bcol)
{

	std::shared_ptr<FrameLL> current_element = GetFrameList();
	while (current_element != NULL) {
		int colour = (current_element->data == GetActiveFrame()) ? 0xFFFFFF : fcol;

		if (current_element->data->IsVisible()) {
			voxieBox::Instance().Debug(x, y, colour, bcol, "* - %s", current_element->data->GetFrameName().c_str());
		}
		else {
			voxieBox::Instance().Debug(x, y, colour, bcol, "  - %s", current_element->data->GetFrameName().c_str());
		}
		current_element = current_element->next;
		y += 10;
	}
}

std::shared_ptr<Bindable> Scene::GetBindings()
{
	return Bindings;
}

void Scene::BuildBindings()
{
	// Keybindings

	// Get Key Down
	Bindings->AddBinding("LastActiveFrame", Binding(std::bind(&voxieBox::GetKeyDown, &voxieBox::Instance(), K_F1, NONE),
		std::bind(&Scene::LastActiveFrame, this)));

	Bindings->AddBinding("NextActiveFrame", Binding(std::bind(&voxieBox::GetKeyDown, &voxieBox::Instance(), K_F2, NONE),
		std::bind(&Scene::NextActiveFrame, this)));

	Bindings->AddBinding("ToggleFrameVisible", Binding(std::bind(&voxieBox::GetKeyDown, &voxieBox::Instance(), K_F3, NONE),
		std::bind(&Scene::ToggleFrameVisible, this)));

	Bindings->AddBinding("ToggleFrameFlattened", Binding(std::bind(&voxieBox::GetKeyDown, &voxieBox::Instance(), K_F10, NONE),
		std::bind(&Scene::ToggleFrameFlattened, this)));


	Bindings->AddBinding("NextElement", Binding(std::bind(&voxieBox::GetKeyDown, &voxieBox::Instance(), K_Arrow_Down, NONE),
		std::bind(&Scene::NextElement, this)));

	Bindings->AddBinding("LastElement", Binding(std::bind(&voxieBox::GetKeyDown, &voxieBox::Instance(), K_Arrow_Up, NONE),
		std::bind(&Scene::LastElement, this)));


	Bindings->AddBinding("MoveFrameUp", Binding(std::bind(&voxieBox::GetKeyDown, &voxieBox::Instance(), K_PageUp, NONE),
		std::bind(&Scene::MoveFrameUp, this)));

	Bindings->AddBinding("MoveFrameDown", Binding(std::bind(&voxieBox::GetKeyDown, &voxieBox::Instance(), K_PageDown, NONE),
		std::bind(&Scene::MoveFrameDown, this)));

	Bindings->AddBinding("SaveFrame", Binding(std::bind(&voxieBox::GetKeyDown, &voxieBox::Instance(), K_F7, CTRL),
		std::bind(&Scene::SaveFrame, this)));

	Bindings->AddBinding("LoadElement", Binding(std::bind(&voxieBox::GetKeyDown, &voxieBox::Instance(), K_J, CTRL),
		std::bind(&Scene::LoadElement, this)));

	Bindings->AddBinding("LoadFrame", Binding(std::bind(&voxieBox::GetKeyDown, &voxieBox::Instance(), K_O, CTRL),
		std::bind(&Scene::LoadFrame, this)));

	Bindings->AddBinding("NewFrame", Binding(std::bind(&voxieBox::GetKeyDown, &voxieBox::Instance(), K_N, CTRL),
		std::bind(&Scene::NewFrame, this)));

	Bindings->AddBinding("SaveScene", Binding(std::bind(&voxieBox::GetKeyDown, &voxieBox::Instance(), K_S, CTRL),
		std::bind(&Scene::SaveScene, this)));

	// Stream Bindings
	Bindings->AddBinding("RenameFrame", StreamBinding(std::bind(&voxieBox::GetKeyDown, &voxieBox::Instance(), K_R, CTRL),
		std::bind(&Scene::RenameFrame, this, std::placeholders::_1)));


	// Get Key
	Bindings->AddBinding("MoveElementBackward", Binding(std::bind(&voxieBox::GetKey, &voxieBox::Instance(), K_W, NONE),
		std::bind(&Scene::MoveElementBackward, this)));

	Bindings->AddBinding("MoveElementForward", Binding(std::bind(&voxieBox::GetKey, &voxieBox::Instance(), K_S, NONE),
		std::bind(&Scene::MoveElementForward, this)));

	Bindings->AddBinding("MoveElementLeft", Binding(std::bind(&voxieBox::GetKey, &voxieBox::Instance(), K_D, NONE),
		std::bind(&Scene::MoveElementLeft, this)));

	Bindings->AddBinding("MoveElementRight", Binding(std::bind(&voxieBox::GetKey, &voxieBox::Instance(), K_A, NONE),
		std::bind(&Scene::MoveElementRight, this)));


	
}
