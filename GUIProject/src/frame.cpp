#include "frame.h"
#include <ostream>
#include <fstream>
#include <windows.h>

Frame::Frame(int frame_layer, const std::string name)
{	
	frame_image = std::make_shared<UIElement>(std::string(""), (0.35f - 0.1f*frame_layer));

	layer = frame_layer;
	frame_name = (std::string)name;
}

Frame::Frame(json source_data)
{

	source_data.at("visible").get_to(this->visible);
	source_data.at("layer").get_to(this->layer);
	source_data.at("name").get_to(this->frame_name);

	frame_image = std::make_shared<UIElement>(std::string(""), (0.35f - 0.1f*this->layer));

	for (json element : source_data["elements"]) {
		if (element_head == NULL) {
			element_head = std::make_shared<UIElementLL>();
			
			element_head->data = std::make_shared<UIElement>(element, 0.35f - 0.1f*layer);
			element_head->next = NULL;
			element_head->last = NULL;
			active_element = element_head;
		}
		else {
			std::shared_ptr<UIElementLL> current = element_head;
			while (current->next != NULL) {
				current = current->next;
			}

			current->next = std::make_shared<UIElementLL>();
			current->next->data = std::make_shared<UIElement>(element, 0.35f - 0.1f*layer);
			current->next->last = current;
			current->next->next = NULL;
		}
	}

	MergeElements();
	show_flattened = true;
}


void Frame::MergeElements()
{
	if (active_element == NULL) return;
	ClearFrame();

	std::shared_ptr<UIElementLL> current = element_head;

	while (current != NULL) {
		if (current->data->visible) {
			MergeElementToFrame(current->data);
		}
		current = current->next;
	}
}

void Frame::AddElement(const std::string texture_path)
{
	if (element_head == NULL) {
		element_head = std::make_shared<UIElementLL>();
		element_head->data = std::make_shared <UIElement>(texture_path, 0.35f - 0.1f*layer);
		element_head->next = NULL;
		element_head->last = NULL;
		active_element = element_head;
	}
	else {
		std::shared_ptr<UIElementLL> current = element_head;
		while (current->next != NULL) {
			current = current->next;
		}
		
		current->next = std::make_shared<UIElementLL>();
		current->next->data = std::make_shared<UIElement>(texture_path, 0.35f - 0.1f*layer);
		current->next->last = current;
		current->next->next = NULL;
	}
}

std::shared_ptr<UIElementLL> Frame::GetElementList()
{
	return element_head;
}

std::shared_ptr<UIElementLL> Frame::GetActiveElement()
{
	return active_element;
}

std::shared_ptr <UIElement> Frame::GetFrame()
{
	return frame_image;
}

void Frame::NextActiveElement()
{
	if (active_element == NULL) active_element = element_head;
	else if (active_element->next != NULL) active_element = active_element->next;
	else active_element = element_head;
}

void Frame::LastActiveElement()
{
	if (active_element == NULL) active_element = element_head;
	else if (active_element->last != NULL) active_element = active_element->last;
	else while (active_element->next) active_element = active_element->next;
}

void Frame::ToggleActiveVisible()
{
	if (active_element) {
		active_element->data->visible = !active_element->data->visible;
	}
}

void Frame::MoveActiveRight()
{
	if (active_element && active_element->data->visible) {
		active_element->data->MoveRight();
	}
}

void Frame::MoveActiveLeft()
{
	if (active_element && active_element->data->visible) {
		active_element->data->MoveLeft();
	}
}

void Frame::MoveActiveBackward()
{
	if (active_element && active_element->data->visible) {
		active_element->data->MoveBackward();
	}
}

void Frame::MoveActiveForward()
{
	if (active_element && active_element->data->visible) {
		active_element->data->MoveForward();
	}
}

void Frame::UpdateActive()
{
	if (active_element != NULL) {
		active_element->data->Update();
	}
}

void Frame::ToggleVisible()
{
	visible = !visible;
}

bool Frame::IsVisible()
{
	return visible;
}

bool Frame::IsShowFlattened()
{
	return show_flattened;
}

const std::string Frame::GetActiveElementPath()
{
	if (active_element) {

		return active_element->data->GetTexturePath();
	}
	else
	{
		return "<NONE>";
	}
}

const std::string Frame::GetFrameName()
{
	return frame_name;
}

void Frame::SetFrameName(std::string name)
{
	frame_name = name;
}

void Frame::ToggleShowFlattened()
{
	show_flattened = !show_flattened;
}

void Frame::ClearFrame()
{
	frame_image = std::make_shared<UIElement>(std::string(""), (0.35f - 0.1f*layer));
}

void Frame::MergeElementToFrame(std::shared_ptr<UIElement> element)
{
	vector2 element_loc = element->GetLocation();
	vector2 element_size = element->GetSize();
	unsigned char* element_data = element->GetPixelData();

	for (int element_x = 0; element_x < element_size.x; element_x++) {
		int pixel_x = element_x + element_loc.x;
		if (pixel_x >= MAX_X) continue; // Out of Frame

		int x_element_offset = element_x * BYTES_PER_PIXEL;
		int x_frame_offset = pixel_x * BYTES_PER_PIXEL;

		for (int element_y = 0; element_y < element_size.y; element_y++) {
			int pixel_y = element_y + element_loc.y;
			if (pixel_y >= MAX_Y) continue; // Out of Frame

			int y_element_offset = element->GetLineByteWidth() * element_y;
			int y_frame_offset = frame_image->GetLineByteWidth() * pixel_y;

			int element_offset = x_element_offset + y_element_offset;
			int frame_offset = x_frame_offset + y_frame_offset;			

			frame_image->GetPixelData()[frame_offset + 0] = element_data[element_offset + 0];
			frame_image->GetPixelData()[frame_offset + 1] = element_data[element_offset + 1];
			frame_image->GetPixelData()[frame_offset + 2] = element_data[element_offset + 2];
			frame_image->GetPixelData()[frame_offset + 3] = element_data[element_offset + 3];
		}
	}

	flattened = true;
}

void Frame::SaveFrame() {
	MergeElements();

	std::string filepath;

	const int len = 255;
	char pBuf[len];
	char drive[5];
	char path[len];

	int bytes = GetModuleFileName(NULL, pBuf, len);
	if (bytes == 0) {
		filepath = ".";
	}
	else {
		
		_splitpath_s((const char*)pBuf, drive, 5, path, len, NULL, 0, NULL, 0);
	}

	const std::string out_file_path = std::string(drive) + std::string(path) + frame_name + ".json";
	MessageBox(0, out_file_path.c_str(), "", MB_OK);

	std::ofstream out_file(out_file_path);

	out_file << ToJson();
}

void Frame::ActiveUpList()
{
	if (active_element == NULL || active_element->last == NULL) return;

	std::shared_ptr <UIElementLL> last_element = active_element->last;

	// Remove Active Element
	if (active_element->next != NULL) {
		last_element->next = active_element->next;
		last_element->next->last = last_element;
	}
	else {
		last_element->next = NULL;
	}

	// Fit Active Between Elements (next & next->next)
	active_element->next = last_element;
	active_element->last = last_element->last;

	if (active_element->last != NULL) {
		active_element->last->next = active_element;
	}
	else {
		element_head = active_element;
	}

	last_element->last = active_element;
}

json Frame::ToJson()
{
	json j;
	j["visible"] = visible;
	j["layer"] = layer;
	j["name"] = frame_name;

	j["elements"] = json();
	
	// Reverse elements to keep order straight
	std::shared_ptr <UIElementLL> current = element_head;
	while (current) {
		j["elements"].push_back(current->data->ToJson());
		current = current->next;
	}

	return j;
}

void Frame::Draw()
{	
	if (!IsVisible()) return;


	if (IsShowFlattened()) {
		GetFrame()->Draw();
	}
	else {
		std::shared_ptr <UIElementLL> current = GetElementList();

		while (current != NULL) {
			current->data->Draw();
			current = current->next;
		}
	}
}

void Frame::Report(int x, int y, int fcol, int bcol)
{
	voxieBox::Instance().Debug(x, y, fcol, bcol, "Frame: %s", GetFrameName().c_str());

	std::shared_ptr <UIElementLL> current_element = GetElementList();

	while (current_element != NULL) {
		y += 10;
		int colour = (current_element == GetActiveElement()) ? 0xFFFFFF : fcol;

		if (current_element->data->visible) {
			voxieBox::Instance().Debug(x, y, colour, bcol, "* - %s", current_element->data->GetTexturePath().c_str());
		}
		else {
			voxieBox::Instance().Debug(x, y, colour, bcol, "  - %s", current_element->data->GetTexturePath().c_str());
		}
		current_element = current_element->next;
	}
}

void Frame::ActiveDownList()
{
	if (active_element == NULL || active_element->next == NULL) return;

	std::shared_ptr <UIElementLL> next_element = active_element->next;

	// Remove Active Element
	if (active_element->last != NULL) {
		next_element->last = active_element->last;
		next_element->last->next = next_element;
	}
	else {
		element_head = next_element;
		next_element->last = NULL;
	}

	// Fit Active Between Elements (next & next->next)
	active_element->last = next_element;
	active_element->next = next_element->next;

	if (active_element->next != NULL) {
		active_element->next->last = active_element;
	}

	next_element->next = active_element;
}
