#pragma once

#include <string>
#include <Windows.h>
#include <Commdlg.h>

enum file_types {
	ERR,
	IMAGE,
	JSON
};

std::string GetFileName(const std::string & prompt, const file_types file_type);