#include "file_io.h"


std::string GetFileName(const std::string & prompt, const file_types file_type) {
	const char * filter;
	switch (file_type) {
	case IMAGE:
		filter = "Images\0*.png;*.jpg;*.gif;*.bmp\0\0";
		break;
	case JSON:
		filter = "Json\0*.json\0\0";
		break;
	default:
		return "";
		break;
	}


	const int BUFSIZE = 1024;
	char * buffer = new char[BUFSIZE]();
	// char buffer[BUFSIZE] = { 0 };
	OPENFILENAME ofns = { 0 };
	ofns.lStructSize = sizeof(ofns);

	ofns.lpstrFilter = filter;
	ofns.lpstrFile = buffer;
	ofns.nMaxFile = BUFSIZE;
	ofns.lpstrTitle = prompt.c_str();
	ofns.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST;
	GetOpenFileName(&ofns);
	return buffer;
}