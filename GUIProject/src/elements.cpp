#include "elements.h"
#include <cstring>
UIElement::UIElement(const std::string texture_name, float z_level)
{
	base_level = z_level;
	// Flat Plane covering scene
	poltex[0].x = -1;	poltex[0].y = -1;		poltex[0].z = base_level;	poltex[0].u = 0; poltex[0].v = 0;
	poltex[1].x = 1;	poltex[1].y = -1; 		poltex[1].z = base_level;	poltex[1].u = 1; poltex[1].v = 0;
	poltex[2].x = 1;	poltex[2].y = 1;		poltex[2].z = base_level;	poltex[2].u = 1; poltex[2].v = 1;
	poltex[3].x = -1;	poltex[3].y = 1;		poltex[3].z = base_level;	poltex[3].u = 0; poltex[3].v = 1;

	// Base Colour of Plane
	poltex[0].col = poltex[1].col = poltex[2].col = poltex[3].col = 0;

	// Make Location / Size reflect full spread
	location.x = location.y = 0;

	// Create Texture 
	if (texture_name == "") {
		texture = std::make_shared<UIImage>();
	}
	else {
		texture = std::make_shared<UIImage>(texture_name);
	}

	size.x = texture->tt->x;
	size.y = texture->tt->y;

	Update();
}

UIElement::UIElement(const json source_json)
{
	source_json.at("base_level").get_to(base_level);
	// Flat Plane covering scene
	poltex[0].x = -1;	poltex[0].y = -1;		poltex[0].z = base_level;	poltex[0].u = 0; poltex[0].v = 0;
	poltex[1].x = 1;	poltex[1].y = -1; 		poltex[1].z = base_level;	poltex[1].u = 1; poltex[1].v = 0;
	poltex[2].x = 1;	poltex[2].y = 1;		poltex[2].z = base_level;	poltex[2].u = 1; poltex[2].v = 1;
	poltex[3].x = -1;	poltex[3].y = 1;		poltex[3].z = base_level;	poltex[3].u = 0; poltex[3].v = 1;

	// Base Colour of Plane
	poltex[0].col = poltex[1].col = poltex[2].col = poltex[3].col = 0;

	source_json.at("visible").get_to(visible);
	source_json.at("location")[0].get_to(location.x);
	source_json.at("location")[1].get_to(location.y);

	source_json.at("size")[0].get_to(size.x);
	source_json.at("size")[1].get_to(size.y);

	std::string path;
	source_json.at("texture_path").get_to(path);
	texture = std::make_shared<UIImage>(path);

	Update();
}

UIElement::~UIElement()
{	
}

void UIElement::Update()
{
	// Location should always be 1 pixel from far edge
	location.x = max(min(location.x, MAX_X - 1), 0);
	location.y = max(min(location.y, MAX_Y - 1), 0);

	// Size should always be a minimum of 1 pixel
	size.x = max(min(size.x, MAX_X), 1);
	size.y = max(min(size.y, MAX_Y), 1);

	// Draw (Only used while in seperate layer mode)
	// x = (y - 256)/256

	// Top Left

	poltex[0].x = PixelToFloat(location.x);
	poltex[0].y = PixelToFloat(location.y);

	// Top Right
	poltex[1].x = PixelToFloat(location.x + size.x);
	poltex[1].y = poltex[0].y;

	// Bottom Left
	poltex[3].x = poltex[0].x;
	poltex[3].y = PixelToFloat(location.y + size.y);

	// Bottom Right
	// Top Right
	poltex[2].x = poltex[1].x;
	poltex[2].y = poltex[3].y;
}

const std::string UIElement::GetTexturePath()
{
	return this->texture->texture_path;
}

poltex_t * UIElement::GetPoltexPtr()
{
	return poltex;
}

std::shared_ptr<tiletype> UIElement::GetTTPtr()
{
	return texture->tt;
}

bool UIElement::isTTLoaded()
{
	return (texture->tt.use_count());
}

vector2 UIElement::GetLocation()
{
	return location;
}

vector2 UIElement::GetSize()
{
	return size;
}

int UIElement::GetLineByteWidth()
{
	return texture->tt->p;
}

unsigned char * UIElement::GetPixelData()
{
	return (unsigned char*)texture->tt->f;
}

void UIElement::SaveElement(std::string filename)
{
	texture->SaveImage(filename);
}

json UIElement::ToJson()
{
	json j;

	j["visible"] = visible;
	j["location"] = { location.x, location.y };
	j["size"] = { size.x, size.y };
	j["texture_path"] = texture->texture_path;
	j["base_level"] = base_level;

	return j;
}

void UIElement::Draw()
{
	if (!visible) return;
	
	voxieBox& vb = voxieBox::Instance();

	vb.DrawPlane(texture->tt, poltex);
}

void UIElement::MoveBackward()
{
	location.y--;
}

void UIElement::MoveForward()
{
	location.y++;
}

void UIElement::MoveLeft()
{
	location.x++;
}

void UIElement::MoveRight()
{
	location.x--;
}

float UIElement::PixelToFloat(int pixel)
{
	return (pixel - 256.f) / 256.f;
}
