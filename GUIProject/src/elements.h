#pragma once

#include "images.h"
#include "voxietypes.h"
#include "voxiebox.h"

#include <string>
#include "../libs/json.hpp"

using json = nlohmann::json;

const int UIElementMesh[] = { 3, 2, 1, 0, -1 };

struct vector2 {
	int x = 0;
	int y = 0;
};

class UIElement {
public:

	bool visible = 1;
	vector2 location;

	UIElement(const std::string texture_name, float z_level = 0);
	UIElement(const json source_json);

	~UIElement();

	void Update();

	const std::string GetTexturePath();
	poltex_t* GetPoltexPtr();
	std::shared_ptr<tiletype> GetTTPtr();

	bool isTTLoaded();
	vector2 GetLocation();
	vector2 GetSize();

	int GetLineByteWidth();

	unsigned char* GetPixelData();

	void SaveElement(std::string filename);

	json ToJson();

	void Draw();

	void MoveBackward();
	void MoveForward();
	void MoveLeft();
	void MoveRight();

	
private:
	float PixelToFloat(int pixel);

	float base_level = 0;
	poltex_t poltex[4];
	std::shared_ptr<UIImage> texture;
	vector2 size;
};

struct UIElementLL {
	std::shared_ptr<UIElement> data = NULL;
	std::shared_ptr<UIElementLL> next = NULL;
	std::shared_ptr<UIElementLL> last = NULL;
};