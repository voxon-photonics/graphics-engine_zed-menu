	
#if 0
!if 1

	#Visual C makefile:

zGfx.exe: zGfx.cpp voxiebox.cpp elements.cpp frame.cpp images.cpp scene.cpp; cl /TP zGfx.cpp voxiebox.cpp elements.cpp frame.cpp images.cpp scene.cpp /Ox /MT /EHsc /link user32.lib comdlg32.lib
	del *.obj

!else

	#GNU C makefile:
zGfx.exe: zGfx.c; gcc zGfx.c -o zGfx.exe -pipe -O3 -s -m64

!endif
!if 0
#endif

#include "voxiebox.h"
#include "./input/input.h"
#include "./input/voxie_input.h"
#include "scene.h"

#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <string>

#include <functional>
// put global structs here

static int gcnti[2], gbstat = 0;

const int DEBUG = 1;


int WINAPI WinMain (HINSTANCE hinst, HINSTANCE hpinst, LPSTR cmdline, int ncmdshow)
{



	voxieBox& voxie = voxieBox::Instance();

	vx::Input input;

	// double tim = 0.0, otim, dtim,  avgdtim = 0.0, keyDly = 0;

	Scene* scene = new Scene();
	input.AddBinding(scene->GetBindings());

	VoxieInput* vi = new VoxieInput();
	input.AddBinding(vi->GetBindings());

	// Initialise Base Frame
	{
		std::string frame_name = "Base Frame";
		scene->AddFrame(frame_name, 0);
		scene->GetActiveFrame()->MergeElements();
		scene->GetActiveFrame()->ToggleShowFlattened();
	}

	while (voxie.Breath()) // Breath is the update loop
	{
		// otim = tim; tim = voxieBox::Instance().FrameTime(); dtim = tim-otim; // the timer

		input.Test();

		voxie.updateControllers(); 
		voxie.updateMouse();

		voxie.StartFrame();



		// test controllers
		
		point3d xx = { -1,-1,0 };
		voxie.reportControllers(xx);
		xx.y = 0.5;
		voxie.reportMouse(xx);


		/*********************
		**	 DRAW GFX PHASE **
		*********************/


			
		scene->Draw();
		
		/*******************
		*     Debug        *
		*******************/

		// final update loop for frame
		if (DEBUG) {
			// avgdtim += (dtim-avgdtim)*.1;

			if (scene->GetActiveFrame() != NULL) {
				voxie.Debug(30, 68, 0xffc080, -1, "Active Element: %s", scene->GetActiveFrame()->GetActiveElementPath().c_str());
				scene->GetActiveFrame()->Report(30, 78, 0xffc080, -1);

				voxie.Debug(1300, 68, 0xffc080, -1, "Active Frame: %s", scene->GetActiveFrame()->GetFrameName().c_str());
			}
			
			scene->Report(1300, 78, 0xffc080, -1);

			voxie.PrintCtrl(30, 600);
		}
		
		voxie.EndFrame();
	}

	voxie.Shutdown();

	return 0;
}

#if 0
!endif
#endif

