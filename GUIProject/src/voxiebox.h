#pragma once

#include <memory>

#include "voxietypes.h"
#include "./input/input_types.h"

// voxieBox is a singleton

class voxieBox final {
	bool success = false;
public:
#pragma region SingletonPattern
	static voxieBox& Instance();

#pragma endregion

	bool GetKey(Keys key, Key_Modifier modifier = NONE);
	bool GetKeyDown(Keys key, Key_Modifier modifier = NONE);
	void ClearInputBuffer();
	char GetInputBuffer();

	void DrawPlane(std::shared_ptr<tiletype> tile_type, poltex_t* poltex_ptr);

	bool Breath();
	void StartFrame();
	void EndFrame();
	void Quit();

	float FrameTime();
	float NowTime();

	void Display2D();
	void Display3D();

	void voxie_printalph_(point3d *p, point3d *r, point3d *d, int col, const char *fmt, ...);
	void Debug(int x, int y, int fcol, int bcol, const char *fmt, ...);

	void PrintCtrl(int x, int y);

	void Shutdown();
	// Matt added functions

	// get the aspectRatio of X,Y or Z
	float getAspectX();
	float getAspectY();
	float getAspectZ();

	// Controller functions

	void updateControllers(); 	// updates controller inputs

	void reportControllers(point3d position); // debug for controllers lists all connected controllers. Set a position to display info

	int  getButtonState(int controller_no);
	bool getButtonOnDown(int controller_no, int button_no);
	bool getButtonIsDown(int controller_no, int button_no);
	bool getButtonOnUp(int controller_no, int button_no);
	int  getAnalogAxis(int controller_no, ControllerAxis axis_name);
	int  getAnalogTrig(int controller_no, ControllerTrigger trigger_name);
	int  getHat(int controller_no);

	int getNumberControllers();

	// Draw Simple Graphics
	void DrawSphere(float x, float y, float z, float radius, int fill, int col);

	// Sound 

	// Mouse 
	void updateMouse(); // updates mouse inputs need to create a 3d point of the mouse X,Y,Z and pass it through
	void reportMouse(point3d report_position); // reports a debug of mouse info
	float getMouseXDelta();
	float getMouseYDelta();
	float getMouseZDelta();
	point3d getMousePosition();
	int  getMouseButtonState();
	bool getMouseClickIsDown(MouseButton button);
	bool getMouseClickOnDown(MouseButton button);
	bool getMouseClickOnUp(MouseButton button);

	// Raw Functions (REMOVE BEFORE SDK RELEASE)
	void DrawVox(float x, float y, float z, int colour);
	void DrawBox(float, float, float, float, float, float, int, int);
	void DrawLine(float, float, float, float, float, float, int);
	void DrawPolygon(pol_t*, int, int);
	void DrawMeshTex(char*, poltex_t*, int, int*, int, int, int);
	void DrawCone(float, float, float, float, float, float, float, float, int, int);
	void DrawSprite(const char*, point3d*, point3d*, point3d*, point3d*, int);
	void PrintAlpha(point3d*, point3d*, point3d*, int, const char*);
	void DrawCube(point3d*, point3d*, point3d*, point3d*, int, int);
	void DrawHeightMap(char*, point3d*, point3d*, point3d*, point3d*, int, int, int);

private:
#pragma region SingletonPattern
	voxieBox(const voxieBox&) = delete;
	voxieBox& operator=(const voxieBox&) = delete;
	voxieBox(voxieBox&&) = delete;
	voxieBox& operator=(voxieBox&&) = delete;

	voxieBox();
	~voxieBox();
#pragma endregion

	static HINSTANCE hvoxie;
	static voxie_wind_t vw;

	voxie_frame_t vf;
	voxie_inputs_t in;
	point3d mouse;

	// Game controllers details
	voxie_xbox_t controller_input[MAX_CONTROLLERS];
	int old_button_controller_data[MAX_CONTROLLERS];
	int no_controllers = MAX_CONTROLLERS;

	
	int display_volume;

	float time;

	int plane_indices[5] = { 3, 2, 1, 0, -1 };

#pragma region InternalFunctions
	int voxie_load(voxie_wind_t *vw);
	void voxie_uninit(int mode);
#pragma endregion

#pragma region DLL_Functions
	void(__cdecl *voxie_loadini_int)(voxie_wind_t *vw);
	void(__cdecl *voxie_getvw)(voxie_wind_t *vw);
	int(__cdecl *voxie_init)(voxie_wind_t *vw);
	void(__cdecl *voxie_uninit_int)(int);
	void(__cdecl *voxie_mountzip)(char *fnam);
	void(__cdecl *voxie_free)(char *fnam);
	HWND(__cdecl *voxie_gethwnd)(void);
	int(__cdecl *voxie_breath)(voxie_inputs_t *);
	void(__cdecl *voxie_quitloop)(void);
	double(__cdecl *voxie_klock)(void);
	int(__cdecl *voxie_keystat)(int);
	int(__cdecl *voxie_keyread)(void);
	int(__cdecl *voxie_xbox_read)(int id, voxie_xbox_t *vx);
	void(__cdecl *voxie_xbox_write)(int id, float lmot, float rmot);
	int(__cdecl *voxie_nav_read)(int id, voxie_nav_t *nav);
	void(__cdecl *voxie_menu_reset)(int(*menu_update)(int id, char *st, double val, int how, void *userdata), void *userdata, char *bkfilnam);
	void(__cdecl *voxie_menu_addtab)(char *st, int x, int y, int xs, int ys);
	void(__cdecl *voxie_menu_additem)(char *st, int x, int y, int xs, int ys, int id, int type, int down, int col, double v, double v0, double v1, double vstp0, double vstp1);
	void(__cdecl *voxie_menu_updateitem)(int id, char *st, int down, double v);
	void(__cdecl *voxie_doscreencap)(void);
	void(__cdecl *voxie_setview)(voxie_frame_t *vf, float x0, float y0, float z0, float x1, float y1, float z1);
	void(__cdecl *voxie_setmaskplane)(voxie_frame_t *vf, float x0, float y0, float z0, float nx, float ny, float nz);
	int(__cdecl *voxie_frame_start)(voxie_frame_t *vf);
	void(__cdecl *voxie_frame_end)(void);
	void(__cdecl *voxie_setleds)(int id, int r, int g, int b);
	void(__cdecl *voxie_drawvox)(voxie_frame_t *vf, float fx, float fy, float fz, int col);
	void(__cdecl *voxie_drawbox)(voxie_frame_t *vf, float x0, float y0, float z0, float x1, float y1, float z1, int fillmode, int col);
	void(__cdecl *voxie_drawlin)(voxie_frame_t *vf, float x0, float y0, float z0, float x1, float y1, float z1, int col);
	void(__cdecl *voxie_drawpol)(voxie_frame_t *vf, pol_t *pt, int n, int col);
	//void (__cdecl *voxie_drawmesh   )(voxie_frame_t *vf, point3dcol_t *vt, int vtn, int *mesh, int meshn, int fillmode, int col); //use voxie_drawmeshtex() instead
	void(__cdecl *voxie_drawmeshtex)(voxie_frame_t *vf, char *fnam, poltex_t *vt, int vtn, int *mesh, int meshn, int flags, int col);
	void(__cdecl *voxie_drawsph)(voxie_frame_t *vf, float fx, float fy, float fz, float rad, int issol, int col);
	void(__cdecl *voxie_drawcone)(voxie_frame_t *vf, float x0, float y0, float z0, float r0, float x1, float y1, float z1, float r1, int fillmode, int col);
	int(__cdecl *voxie_drawspr)(voxie_frame_t *vf, const char *fnam, point3d *p, point3d *r, point3d *d, point3d *f, int col);
	int(__cdecl *voxie_drawspr_ext)(voxie_frame_t *vf, const char *fnam, point3d *p, point3d *r, point3d *d, point3d *f, int col, float forcescale, float fdrawratio);
	void(__cdecl *voxie_printalph)(voxie_frame_t *vf, point3d *p, point3d *r, point3d *d, int col, const char *st);
	void(__cdecl *voxie_drawcube)(voxie_frame_t *vf, point3d *p, point3d *r, point3d *d, point3d *f, int fillmode, int col);
	float(__cdecl *voxie_drawheimap)(voxie_frame_t *vf, char *fnam, point3d *p, point3d *r, point3d *d, point3d *f, int colorkey, int reserved, int flags);
	void(__cdecl *voxie_drawdicom)(voxie_frame_t *vf, voxie_dicom_t *vd, const char *gfilnam, point3d *gp, point3d *gr, point3d *gd, point3d *gf, int *animn, int *loaddone);
	void(__cdecl *voxie_debug_print6x8)(int x, int y, int fcol, int bcol, const char *st);
	void(__cdecl *voxie_debug_drawpix)(int x, int y, int col);
	void(__cdecl *voxie_debug_drawhlin)(int x0, int x1, int y, int col);
	void(__cdecl *voxie_debug_drawline)(float x0, float y0, float x1, float y1, int col);
	void(__cdecl *voxie_debug_drawcirc)(int xc, int yc, int r, int col);
	void(__cdecl *voxie_debug_drawrectfill)(int x0, int y0, int x1, int y1, int col);
	void(__cdecl *voxie_debug_drawcircfill)(int x, int y, int r, int col);
	int(__cdecl *voxie_playsound)(const char *fnam, int chan, int volperc0, int volperc1, float frqmul);
	void(__cdecl *voxie_playsound_update)(int handle, int chan, int volperc0, int volperc1, float frqmul);
	void(__cdecl *voxie_setaudplaycb)(void(*userplayfunc)(int *samps, int nframes));
	void(__cdecl *voxie_setaudreccb)(void(*userrecfunc)(int *samps, int nframes));
	int(__cdecl *voxie_rec_open)(voxie_rec_t *vr, char *fnam, int flags);
	int(__cdecl *voxie_rec_play)(voxie_rec_t *vr, int domode);
	void(__cdecl *voxie_rec_close)(voxie_rec_t *vr);
	//High-level (easy) picture loading function:
	void(__cdecl *kpzload)(const char *, INT_PTR *, int *, int *, int *);
	//Low-level PNG/JPG functions:
	int(__cdecl *kpgetdim)(const char *, int, int *, int *);
	int(__cdecl *kprender)(const char *, int, INT_PTR, int, int, int, int, int);
	//Ken's ZIP functions:
	int(__cdecl *kzaddstack)(const char *);
	void(__cdecl *kzuninit)();
	void(__cdecl *kzsetfil)(FILE *);
	INT_PTR(__cdecl *kzopen)(const char *);
	void(__cdecl *kzfindfilestart)(const char *);
	int(__cdecl *kzfindfile)(char *);
	int(__cdecl *kzread)(void *, int);
	int(__cdecl *kzfilelength)();
	int(__cdecl *kzseek)(int, int);
	int(__cdecl *kztell)();
	int(__cdecl *kzgetc)();
	int(__cdecl *kzeof)();
	void(__cdecl *kzclose)();
#pragma endregion
};

