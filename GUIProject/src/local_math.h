#pragma once
#include "voxiebox.h"
#include <math.h>
	
#define PI 3.14159265358979323

#ifndef min
#define min(a,b) (((a)<(b))?(a):(b))
#endif
#ifndef max
#define max(a,b) (((a)<(b))?(a):(b))
#endif


//Rotate vectors a & b around their common plane, by ang
static void rotvex(float ang, point3d *a, point3d *b)
{
	float f, c, s;

	c = cos(ang); s = sin(ang);
	f = a->x; a->x = f * c + b->x*s; b->x = b->x*c - f * s;
	f = a->y; a->y = f * c + b->y*s; b->y = b->y*c - f * s;
	f = a->z; a->z = f * c + b->z*s; b->z = b->z*c - f * s;
}