#include "images.h"

#define STB_IMAGE_IMPLEMENTATION
#include "../libs/stb_image.h"
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include "../libs/stb_image_resize.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../libs/stb_image_write.h"

UIImage::UIImage()
{
	tt = std::make_shared<tiletype>();

	tt->x = MAX_X;
	tt->y = MAX_Y;
	tt->p = tt->x * CHANNELS_PER_PIXEL;
	tt->f = (__int64)(new unsigned char[MAX_X * MAX_X * BYTES_PER_PIXEL]());
}

UIImage::UIImage(const std::string filePath)
{
	int width, height;

	texture_path = filePath;

	unsigned char * data = stbi_load(filePath.c_str(), &width, &height, &bytes_per_pixel, CHANNELS_PER_PIXEL);


	// Resize UIImage if too large for frame
	if (width > MAX_X || height > MAX_Y) {
		int new_x, new_y;
		if (width > height) {
			new_x = MAX_X;
			new_y = (int)((float)MAX_X / width * height);
		}
		else {
			new_y = MAX_Y;
			new_x = (int)((float)MAX_Y / height * width);
		}

		unsigned char * new_data = new unsigned char[new_x * new_y * BYTES_PER_PIXEL]();

		if (stbir_resize_uint8(data, width, height, 0, new_data, new_x, new_y, 0, 4))
		{
			delete(data);
			data = new_data;
			width = new_x;
			height = new_y;
		}
	}


	char buf;
	for (int x = 0; x < width; x++) {
		int x_offset = x * 4;
		for (int y = 0; y < height; y++) {
			int y_offset = BYTES_PER_PIXEL * width * y;
			int offset = x_offset + y_offset;

			buf = data[offset + 0];
			data[offset + 0] = data[offset + 2];
			data[offset + 2] = buf;
		}
	}

	tt = std::make_shared<tiletype>();
	tt->f = (__int64)data;
	tt->x = width;
	tt->y = height;
	tt->p = width * BYTES_PER_PIXEL;
}

UIImage::~UIImage()
{
	stbi_image_free((void *)tt->f);
}

void UIImage::SaveImage(std::string filename)
{
	stbi_write_png(filename.c_str(), tt->x, tt->y, CHANNELS_PER_PIXEL, (void *)tt->f, 0);
}
