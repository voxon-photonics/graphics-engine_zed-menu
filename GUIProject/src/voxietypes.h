#pragma once
#pragma pack(push,1)

//--------------------------------------------------------------------------------------------------
#define MAXDISP 3
//Graphics (medium level)
#define FILLMODE_DOT  0
#define FILLMODE_LINE 1
#define FILLMODE_SURF 2
#define FILLMODE_SOL  3
#define VOXIE_DICOM_MIPS 4 //NOTE:limited to 6 by dicom_gotz

#if defined(_WIN32)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#else
#define INT_PTR int
#endif

#include <stdarg.h>
#include <stdio.h>

typedef struct { float x, y; } point2d;
typedef struct
{
	point2d keyst[8];
	int colo_r, colo_g, colo_b;
	int mono_r, mono_g, mono_b;
	int mirrorx, mirrory;
} voxie_disp_t;
typedef struct
{
	int useemu; float emuhang, emuvang, emudist;                                                                        //Emulation
	int xdim, ydim, projrate, framepervol, usecol, dispnum, HighLumenDangerProtect; voxie_disp_t disp[MAXDISP];         //Display
	int hwsync_frame0, hwsync_phase, hwsync_amp[4], hwsync_pha[4], hwsync_levthresh, voxie_vol;                         //Actuator
	int ilacemode, drawstroke, dither, smear, usekeystone, flip, menu_on_voxie; float aspx, aspy, aspz, gamma, density; //Render
	int sndfx_vol, voxie_aud, excl_audio, sndfx_aud[2], playsamprate, playnchans, recsamprate, recnchans;               //Audio
	int isrecording, hacks, dispcur;                                                                                    //Misc.
	double freq, phase;                                                                                                 //Obsolete
} voxie_wind_t;
typedef struct { int bstat, obstat, dmousx, dmousy, dmousz; } voxie_inputs_t;
typedef struct { short but, lt, rt, tx0, ty0, tx1, ty1, hat; } voxie_xbox_t;
typedef struct { float dx, dy, dz, ax, ay, az; int but; } voxie_nav_t;
enum { MENU_TEXT = 0, MENU_LINE, MENU_BUTTON, MENU_HSLIDER = MENU_BUTTON + 4, MENU_VSLIDER, MENU_EDIT, MENU_EDIT_DO };
typedef struct { INT_PTR f, p, x, y; } tiletype;
typedef struct
{
	INT_PTR f, p, fp; int x, y, usecol, drawplanes, x0, y0, x1, y1;
	float xmul, ymul, zmul, xadd, yadd, zadd;
	tiletype f2d;
} voxie_frame_t;
typedef struct { float x, y, z; } point3d;
typedef struct { float x, y, z; int p2; } pol_t;
//typedef struct { float x, y, z; int col; } point3dcol_t; //deprecated; use poltex_t/voxie_drawmeshtex() instead
typedef struct { float x, y, z, u, v; int col; } poltex_t;
typedef struct
{
	unsigned short   *mip[VOXIE_DICOM_MIPS]; //3D grid for each mip level; x lsb .. z msb
	unsigned __int64 *bit[VOXIE_DICOM_MIPS]; //3D bit grid for floodfill3d per mip level
	int *gotz;                       //bit array holding which z slices have loaded so far
	point3d rulerpos[3];
	point3d slicep, slicer, sliced, slicef;
	float zscale, timsincelastmove, detail, level[2];
	int xsiz, ysiz, zsiz;            //Dimensions
	int zloaded;                     //number of Z slices loaded so far
	int color[2], autodetail, fillsolid, slicemode, drawstats, ruler;
	int defer_load_posori, gotzip;
	int forcemip, lockmip; //0=not locked, 1-4 = lock mip 0-3
	int saveasstl; //usually 0, set to +1 or -1 to write current levels to STL, function sets this back to 0 when done.
	int dofloodfill; //usually 0, set to 1 to do a floodfill at location rulerpos[0]
	int n, cnt, nummin, nummax; //<- from anim_t
	char filespec[MAX_PATH];    //<- from anim_t
} voxie_dicom_t;
typedef struct
{
	FILE *fil; //Warning: do not use this from user code - for internal use only.
	double timleft;
	float *frametim;
	int *frameseek, framemal, kztableoffs, error;
	int playmode, framecur, framenum;
	int currep, animmode/*0=forward, 1=ping-pong, 2=reverse*/;
	int playercontrol[4];
} voxie_rec_t;

#pragma pack(pop)