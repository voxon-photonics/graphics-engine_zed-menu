#pragma once
#include "voxietypes.h"
#include <string>

// Everything needs to be 4 bit colour for the VX1
const int BYTES_PER_PIXEL = 4;
const int CHANNELS_PER_PIXEL = 4;
const int MAX_X = 512, MAX_Y = 512;

class UIImage {
public:
	std::string texture_path;
	int bytes_per_pixel = 0;
	std::shared_ptr<tiletype> tt;

	UIImage();
	UIImage(const std::string filePath);
	~UIImage();
	void SaveImage(std::string filename);

};