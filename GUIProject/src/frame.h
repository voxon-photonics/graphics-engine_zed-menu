#pragma once

#include "elements.h"
#include "images.h"

#include <string>
#include "../libs/json.hpp"

using json = nlohmann::json;

class Frame {
	bool flattened = false;
	bool visible = true;
	bool show_flattened = false;
	int layer = 0;
	std::shared_ptr<UIElement> frame_image;
	std::shared_ptr<UIElementLL> element_head = NULL;
	std::shared_ptr<UIElementLL> active_element = NULL;

	std::string frame_name;

public:
	Frame(int frame_layer, const std::string frame_name);
	Frame(json source_data);
	void MergeElements();
	void AddElement(const std::string texture_path);

	std::shared_ptr <UIElementLL> GetElementList();
	std::shared_ptr <UIElementLL> GetActiveElement();
	std::shared_ptr <UIElement> GetFrame();

	void NextActiveElement();
	void LastActiveElement();

	void ToggleActiveVisible();
	void MoveActiveRight();
	void MoveActiveLeft();
	void MoveActiveBackward();
	void MoveActiveForward();

	void UpdateActive();

	void ToggleVisible();
	bool IsVisible();
	bool IsShowFlattened();

	const std::string GetActiveElementPath();
	const std::string GetFrameName();

	void SetFrameName(std::string name);

	void ToggleShowFlattened();

	void SaveFrame();

	void ActiveDownList();
	void ActiveUpList();

	json ToJson();

	void Draw();
	void Report(int x, int y, int fcol, int bcol);



private:
	void ClearFrame();
	void MergeElementToFrame(std::shared_ptr<UIElement> element);
};

struct FrameLL{
	std::shared_ptr<Frame> data = NULL;
	std::shared_ptr<FrameLL> next = NULL;
	std::shared_ptr<FrameLL> last = NULL;
};