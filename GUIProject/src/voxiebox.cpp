#include "voxiebox.h"
#include <string>

HINSTANCE voxieBox::hvoxie = 0;
voxie_wind_t voxieBox::vw;
//point3d voxieBox::mouse = { 0,0,0 };

voxieBox::voxieBox()
{
	if (success) return;

	if (voxie_load(&vw) < 0) //Load voxiebox.dll and get settings from voxiebox.ini. May override settings in vw structure here if desired.
	{
		MessageBox(0, "Error: can't load voxiebox.dll", "", MB_OK); success = false;
	}
	if (voxie_init(&vw) < 0) //Start video and audio.
	{	MessageBox(0,"Error: voxie_init() failed","",MB_OK); success = false;
	}

	success = true;
}

voxieBox::~voxieBox()
{
}

bool voxieBox::GetKey(Keys key, Key_Modifier modifier)
{
	bool state = true;
	switch (modifier) {
	case ALT:
		state = (voxie_keystat(K_Alt_Left) || voxie_keystat(K_Alt_Right));
		break;
	case CTRL:
		state = (voxie_keystat(K_Control_Left) || voxie_keystat(K_Control_Right));
		break;
	case SHIFT:
		state = (voxie_keystat(K_Shift_Left) || voxie_keystat(K_Shift_Right));
		break;
	case NONE:
		state = !(voxie_keystat(K_Alt_Left) || voxie_keystat(K_Alt_Right) ||
			voxie_keystat(K_Control_Left) || voxie_keystat(K_Control_Right) ||
			voxie_keystat(K_Shift_Left) || voxie_keystat(K_Shift_Right)
		);
	default:
		break;
	}

	return (voxie_keystat(key) && state);
}

bool voxieBox::GetKeyDown(Keys key, Key_Modifier modifier)
{
	bool state = true;
	switch (modifier) {
	case ALT:
		state = (voxie_keystat(K_Alt_Left)>0) || (voxie_keystat(K_Alt_Right)>0);
		break;
	case CTRL:
		state = (voxie_keystat(K_Control_Left)>0) || (voxie_keystat(K_Control_Right)>0);
		break;
	case SHIFT:
		state = (voxie_keystat(K_Shift_Left)>0) || (voxie_keystat(K_Shift_Right)>0);
		break;
	case NONE:
		state = !(voxie_keystat(K_Alt_Left) || voxie_keystat(K_Alt_Right) ||
			voxie_keystat(K_Control_Left) || voxie_keystat(K_Control_Right) ||
			voxie_keystat(K_Shift_Left) || voxie_keystat(K_Shift_Right)
			);
		break;
	default:
		break;
	}

	return (voxie_keystat(key) == 1) && state;
}

void voxieBox::ClearInputBuffer()
{
	while (voxie_keyread());
}

char voxieBox::GetInputBuffer()
{
	return (voxie_keyread() & 0xFF);
}

void voxieBox::DrawPlane(std::shared_ptr<tiletype> tile_type, poltex_t* poltex_ptr)
{
	voxie_drawmeshtex(&vf, (char*)tile_type.get(), poltex_ptr, 5, plane_indices, 6, 2 + (1 << 3), 0x404040);
}

bool voxieBox::Breath()
{
	time = voxie_klock();
	return (voxie_breath(&in) == 0);
}

void voxieBox::StartFrame()
{
	voxie_frame_start(&vf);

	voxie_setview(&vf, -vw.aspx, -vw.aspy, -vw.aspz, vw.aspx, vw.aspy, vw.aspz);

	//draw wireframe box
	voxie_drawbox(&vf, -vw.aspx + 1e-3f, -vw.aspy + 1e-3f, -vw.aspz, +vw.aspx - 1e-3f, +vw.aspy - 1e-3f, +vw.aspz, 1, 0xffffff);
}

void voxieBox::EndFrame()
{	
	voxie_frame_end(); voxie_getvw(&vw);
}

void voxieBox::Quit()
{
	voxie_quitloop();
}

float voxieBox::NowTime()
{
	return voxie_klock();
}

void voxieBox::Display2D()
{
	if(vw.voxie_vol > 0)	display_volume = vw.voxie_vol;
	
	vw.voxie_vol = 0;
	voxie_init(&vw);
}

void voxieBox::Display3D()
{
	if (vw.voxie_vol == 0 && display_volume > 0)	vw.voxie_vol = display_volume;

	voxie_init(&vw);
}

float voxieBox::getAspectX()
{
	return vw.aspx;
}

float voxieBox::getAspectY()
{
	return vw.aspy;
}

float voxieBox::getAspectZ()
{
	return vw.aspz;
}
float voxieBox::FrameTime()
{
	return time;
}

void voxieBox::updateControllers()
{

	// iterate over no_controller to allow dynamic updating of how many controllers are currently plugged in

	for (no_controllers = 0; no_controllers < MAX_CONTROLLERS; no_controllers++)
	{
		old_button_controller_data[no_controllers] = controller_input[no_controllers].but;

		if (!voxie_xbox_read(no_controllers, &controller_input[no_controllers])) break; //but, lt, rt, tx0, ty0, tx1, ty1

	}
}

void voxieBox::reportControllers(point3d position)
{
	int i, col[4] = { 0xffff00,0x00ffff,0xff00ff,0x00ff00 };
	point3d pp, dd, rr;
	pp.z = position.z;  rr.x = 0.04f; dd.x = 0.020f;
	pp.x = position.x;  rr.y = 0.0f; dd.y = 0.080f;
	pp.y = position.y;  rr.z = 0.0f; dd.z = 0.0f;
	
	if (getNumberControllers() <= 0) {
		voxie_printalph_(&pp, &rr, &dd, col[0], "no controllers found, %d", getNumberControllers());

	}


	for (i = 0; i < getNumberControllers(); i++) {

		voxie_printalph_(&pp, &rr, &dd, col[i], "LS X%d Y%d RS X%d Y%d LT%d RT%d",
			getAnalogAxis(i, LEFT_STICK_X), getAnalogAxis(i, LEFT_STICK_Y), getAnalogAxis(i, RIGHT_STICK_X), getAnalogAxis(i, RIGHT_STICK_Y),
			getAnalogTrig(i, LEFT_TRIGGER), getAnalogTrig(i, RIGHT_TRIGGER));
		
		pp.y -= -0.120f;

		voxie_printalph_(&pp, &rr, &dd, col[i], "But %04d 0%d 1%d 2%d 3%d 4%d 5%d 6%d 7%d 8%d",
			getButtonState(i), getButtonIsDown(i, 0), getButtonIsDown(i, 1), getButtonIsDown(i, 2), getButtonIsDown(i, 3), getButtonIsDown(i, 4),
			getButtonIsDown(i, 5), getButtonIsDown(i, 6), getButtonIsDown(i, 7), getButtonIsDown(i, 8));
		
		pp.y -= -0.120f;

		voxie_printalph_(&pp, &rr, &dd, col[i], "9%d 10%d 11%d 12%d 13%d 14%d 15%d HAT: %d",
			getButtonIsDown(i, 9), getButtonIsDown(i, 10), getButtonIsDown(i, 11), getButtonIsDown(i, 12), getButtonIsDown(i, 13),
			getButtonIsDown(i, 14), getButtonIsDown(i, 15), getHat(i));

		pp.y -= -0.120f;

		voxie_printalph_(&pp, &rr, &dd, col[i], "OD: 12%d 13%d OU: 14%d 15%d",
			getButtonOnDown(i, 12), getButtonOnDown(i, 13),
			getButtonOnUp(i, 14), getButtonOnUp(i, 15));
		
		pp.y -= -0.120f;

	}

}

// Gets the raw data for the button states
int voxieBox::getButtonState(int controller_no)
{
	return controller_input[controller_no].but;
}


/* Truth Table for button presses
00 =  not pressed
01 = was pressed false - on up
11 = true - isDown
10 = true - onDowm
*/

bool voxieBox::getButtonOnDown(int controller_no, int button_no)
{
	return ((controller_input[controller_no].but >> button_no) & 1) && !((old_button_controller_data[controller_no] >> button_no) & 1);
}

bool voxieBox::getButtonIsDown(int controller_no, int button_no)
{
	return ((controller_input[controller_no].but >> button_no) & 1);
}

bool voxieBox::getButtonOnUp(int controller_no, int button_no)
{

	return ( !(controller_input[controller_no].but >> button_no) & 1) && ((old_button_controller_data[controller_no] >> button_no) & 1);
}

int voxieBox::getAnalogAxis(int controller_no, ControllerAxis axis_name)
{
	switch (axis_name) {
	case (LEFT_STICK_X):
		return controller_input[controller_no].tx0;
		break;
	case (LEFT_STICK_Y):
		return controller_input[controller_no].ty0;
		break;
	case (RIGHT_STICK_X):
		return controller_input[controller_no].tx1;
		break;
	case (RIGHT_STICK_Y):
		return controller_input[controller_no].ty1;
		break;
	default:
		return 0;
	}
	
}

int voxieBox::getAnalogTrig(int controller_no, ControllerTrigger trigger_name)
{
	switch (trigger_name) {
	case (LEFT_TRIGGER):
		return controller_input[controller_no].lt;
		break;
	case (RIGHT_TRIGGER):
		return controller_input[controller_no].rt;
		break;
	default:
		return 0;
	}
}

int voxieBox::getHat(int controller_no)
{
	return controller_input[controller_no].hat;
}

int voxieBox::getNumberControllers()
{
	return no_controllers;
}

void voxieBox::Shutdown()
{
	voxie_uninit(0);
}


int voxieBox::voxie_load(voxie_wind_t *vw)
{
#if defined(_WIN32)

	hvoxie = LoadLibrary("voxiebox.dll"); 
	
	if (!hvoxie) { 
		const char * result = std::to_string(GetLastError()).c_str(); 
		MessageBox(0, result, "", MB_OK); 
		return(-1); 
	};

	voxie_loadini_int = (void(__cdecl *)(voxie_wind_t*))GetProcAddress(hvoxie, "voxie_loadini_int");
	voxie_getvw = (void(__cdecl *)(voxie_wind_t*))GetProcAddress(hvoxie, "voxie_getvw");
	voxie_init = (int(__cdecl *)(voxie_wind_t*))GetProcAddress(hvoxie, "voxie_init");
	voxie_uninit_int = (void(__cdecl *)(int))GetProcAddress(hvoxie, "voxie_uninit_int");
	voxie_mountzip = (void(__cdecl *)(char*))GetProcAddress(hvoxie, "voxie_mountzip");
	voxie_free = (void(__cdecl *)(char*))GetProcAddress(hvoxie, "voxie_free");
	voxie_gethwnd = (HWND(__cdecl *)(void))GetProcAddress(hvoxie, "voxie_gethwnd");
	voxie_breath = (int(__cdecl *)(voxie_inputs_t*))GetProcAddress(hvoxie, "voxie_breath");
	voxie_quitloop = (void(__cdecl *)(void))GetProcAddress(hvoxie, "voxie_quitloop");
	voxie_klock = (double(__cdecl *)(void))GetProcAddress(hvoxie, "voxie_klock");
	voxie_keystat = (int(__cdecl *)(int))GetProcAddress(hvoxie, "voxie_keystat");
	voxie_keyread = (int(__cdecl *)(void))GetProcAddress(hvoxie, "voxie_keyread");
	voxie_xbox_read = (int(__cdecl *)(int, voxie_xbox_t *))GetProcAddress(hvoxie, "voxie_xbox_read");
	voxie_xbox_write = (void(__cdecl *)(int, float, float))GetProcAddress(hvoxie, "voxie_xbox_write");
	voxie_nav_read = (int(__cdecl *)(int, voxie_nav_t *))GetProcAddress(hvoxie, "voxie_nav_read");
	voxie_menu_reset = (void(__cdecl *)(int(*)(int, char*, double, int, void*), void*, char*))GetProcAddress(hvoxie, "voxie_menu_reset");
	voxie_menu_addtab = (void(__cdecl *)(char*, int, int, int, int))GetProcAddress(hvoxie, "voxie_menu_addtab");
	voxie_menu_additem = (void(__cdecl *)(char*, int, int, int, int, int, int, int, int, double, double, double, double, double))GetProcAddress(hvoxie, "voxie_menu_additem");
	voxie_menu_updateitem = (void(__cdecl *)(int, char*, int, double))GetProcAddress(hvoxie, "voxie_menu_updateitem");
	voxie_doscreencap = (void(__cdecl *)(void))GetProcAddress(hvoxie, "voxie_doscreencap");
	voxie_setview = (void(__cdecl *)(voxie_frame_t*, float, float, float, float, float, float))GetProcAddress(hvoxie, "voxie_setview");
	voxie_setmaskplane = (void(__cdecl *)(voxie_frame_t*, float, float, float, float, float, float))GetProcAddress(hvoxie, "voxie_setmaskplane");
	voxie_frame_start = (int(__cdecl *)(voxie_frame_t*))GetProcAddress(hvoxie, "voxie_frame_start");
	voxie_frame_end = (void(__cdecl *)(void))GetProcAddress(hvoxie, "voxie_frame_end");
	voxie_setleds = (void(__cdecl *)(int, int, int, int))GetProcAddress(hvoxie, "voxie_setleds");
	voxie_drawvox = (void(__cdecl *)(voxie_frame_t*, float, float, float, int))GetProcAddress(hvoxie, "voxie_drawvox");
	voxie_drawbox = (void(__cdecl *)(voxie_frame_t*, float, float, float, float, float, float, int, int))GetProcAddress(hvoxie, "voxie_drawbox");
	voxie_drawlin = (void(__cdecl *)(voxie_frame_t*, float, float, float, float, float, float, int))GetProcAddress(hvoxie, "voxie_drawlin");
	voxie_drawpol = (void(__cdecl *)(voxie_frame_t*, pol_t*, int, int))GetProcAddress(hvoxie, "voxie_drawpol");
	//voxie_drawmesh     = (  void (__cdecl *)(voxie_frame_t*,point3dcol_t*,int,int*,int,int,int))GetProcAddress(hvoxie,"voxie_drawmesh"); //use voxie_drawmeshtex() instead
	voxie_drawmeshtex = (void(__cdecl *)(voxie_frame_t*, char*, poltex_t*, int, int*, int, int, int))GetProcAddress(hvoxie, "voxie_drawmeshtex");
	voxie_drawsph = (void(__cdecl *)(voxie_frame_t*, float, float, float, float, int, int))GetProcAddress(hvoxie, "voxie_drawsph");
	voxie_drawcone = (void(__cdecl *)(voxie_frame_t*, float, float, float, float, float, float, float, float, int, int))GetProcAddress(hvoxie, "voxie_drawcone");
	voxie_drawspr = (int(__cdecl *)(voxie_frame_t*, const char*, point3d*, point3d*, point3d*, point3d*, int))GetProcAddress(hvoxie, "voxie_drawspr");
	voxie_drawspr_ext = (int(__cdecl *)(voxie_frame_t*, const char*, point3d*, point3d*, point3d*, point3d*, int, float, float))GetProcAddress(hvoxie, "voxie_drawspr_ext");
	voxie_printalph = (void(__cdecl *)(voxie_frame_t*, point3d*, point3d*, point3d*, int, const char*))GetProcAddress(hvoxie, "voxie_printalph");
	voxie_drawcube = (void(__cdecl *)(voxie_frame_t*, point3d*, point3d*, point3d*, point3d*, int, int))GetProcAddress(hvoxie, "voxie_drawcube");
	voxie_drawheimap = (float(__cdecl *)(voxie_frame_t*, char*, point3d*, point3d*, point3d*, point3d*, int, int, int))GetProcAddress(hvoxie, "voxie_drawheimap");
	voxie_drawdicom = (void(__cdecl *)(voxie_frame_t*, voxie_dicom_t*, const char *, point3d*, point3d*, point3d*, point3d*, int*, int*))GetProcAddress(hvoxie, "voxie_drawdicom");
	voxie_debug_print6x8 = (void(__cdecl *)(int x, int y, int fcol, int bcol, const char *st))      GetProcAddress(hvoxie, "voxie_debug_print6x8");
	voxie_debug_drawpix = (void(__cdecl *)(int x, int y, int col))                                 GetProcAddress(hvoxie, "voxie_debug_drawpix");
	voxie_debug_drawhlin = (void(__cdecl *)(int x0, int x1, int y, int col))                        GetProcAddress(hvoxie, "voxie_debug_drawhlin");
	voxie_debug_drawline = (void(__cdecl *)(float x0, float y0, float x1, float y1, int col))       GetProcAddress(hvoxie, "voxie_debug_drawline");
	voxie_debug_drawcirc = (void(__cdecl *)(int xc, int yc, int r, int col))                        GetProcAddress(hvoxie, "voxie_debug_drawcirc");
	voxie_debug_drawrectfill = (void(__cdecl *)(int x0, int y0, int x1, int y1, int col))               GetProcAddress(hvoxie, "voxie_debug_drawrectfill");
	voxie_debug_drawcircfill = (void(__cdecl *)(int x, int y, int r, int col))                          GetProcAddress(hvoxie, "voxie_debug_drawcircfill");
	voxie_playsound = (int(__cdecl *)(const char*, int, int, int, float))GetProcAddress(hvoxie, "voxie_playsound");
	voxie_playsound_update = (void(__cdecl *)(int, int, int, int, float))GetProcAddress(hvoxie, "voxie_playsound_update");
	voxie_setaudplaycb = (void(__cdecl *)(void(*userplayfunc)(int*, int)))GetProcAddress(hvoxie, "voxie_setaudplaycb");
	voxie_setaudreccb = (void(__cdecl *)(void(*userrecfunc)(int*, int)))GetProcAddress(hvoxie, "voxie_setaudreccb");
	voxie_rec_open = (int(__cdecl *)(voxie_rec_t*, char*, int))GetProcAddress(hvoxie, "voxie_rec_open");
	voxie_rec_play = (int(__cdecl *)(voxie_rec_t*, int))      GetProcAddress(hvoxie, "voxie_rec_play");
	voxie_rec_close = (void(__cdecl *)(voxie_rec_t*))          GetProcAddress(hvoxie, "voxie_rec_close");
	kpzload = (void(__cdecl *)(const char*, INT_PTR*, int*, int*, int*))        GetProcAddress(hvoxie, "kpzload");
	kpgetdim = (int(__cdecl *)(const char*, int, int*, int*))                  GetProcAddress(hvoxie, "kpgetdim");
	kprender = (int(__cdecl *)(const char*, int, INT_PTR, int, int, int, int, int))GetProcAddress(hvoxie, "kprender");
	kzaddstack = (int(__cdecl *)(const char*))GetProcAddress(hvoxie, "kzaddstack");
	kzuninit = (void(__cdecl *)(void))       GetProcAddress(hvoxie, "kzuninit");
	kzsetfil = (void(__cdecl *)(FILE*))      GetProcAddress(hvoxie, "kzsetfil");
	kzopen = (INT_PTR(__cdecl *)(const char*))GetProcAddress(hvoxie, "kzopen");
	kzfindfilestart = (void(__cdecl *)(const char*))GetProcAddress(hvoxie, "kzfindfilestart");
	kzfindfile = (int(__cdecl *)(char*))      GetProcAddress(hvoxie, "kzfindfile");
	kzread = (int(__cdecl *)(void*, int))  GetProcAddress(hvoxie, "kzread");
	kzfilelength = (int(__cdecl *)(void))       GetProcAddress(hvoxie, "kzfilelength");
	kzseek = (int(__cdecl *)(int, int))    GetProcAddress(hvoxie, "kzseek");
	kztell = (int(__cdecl *)(void))       GetProcAddress(hvoxie, "kztell");
	kzgetc = (int(__cdecl *)(void))       GetProcAddress(hvoxie, "kzgetc");
	kzeof = (int(__cdecl *)(void))       GetProcAddress(hvoxie, "kzeof");
	kzclose = (void(__cdecl *)(void))       GetProcAddress(hvoxie, "kzclose");
#endif

	voxie_loadini_int(vw);
	return(0);
}

void voxieBox::voxie_uninit(int mode)
{
	voxie_uninit_int(mode);
#if defined(_WIN32)
	if (!mode) { if (hvoxie) { FreeLibrary(hvoxie); hvoxie = 0; } }
#endif
}

//Extension for C/C++ allowing use of this function printf-style
void voxieBox::voxie_printalph_(point3d *p, point3d *r, point3d *d, int col, const char *fmt, ...)
{
	va_list arglist;
	char st[1024];

	if (!fmt) return;
	va_start(arglist, fmt);
#if defined(_WIN32)
	if (_vsnprintf((char *)&st, sizeof(st) - 1, fmt, arglist)) st[sizeof(st) - 1] = 0;
#else
	if (vsprintf((char *)&st, fmt, arglist)) st[sizeof(st) - 1] = 0; //FUK:unsafe!
#endif
	va_end(arglist);

	voxie_printalph(&vf, p, r, d, col, st);
}

//Extension for C/C++ allowing use of this function printf-style
void voxieBox::Debug(int x, int y, int fcol, int bcol, const char *fmt, ...)
{
	va_list arglist;
	char st[1024];

	if (!fmt) return;
	va_start(arglist, fmt);
#if defined(_WIN32)
	if (_vsnprintf((char *)&st, sizeof(st) - 1, fmt, arglist)) st[sizeof(st) - 1] = 0;
#else
	if (vsprintf((char *)&st, fmt, arglist)) st[sizeof(st) - 1] = 0; //FUK:unsafe!
#endif
	va_end(arglist);

	voxie_debug_print6x8(x, y, fcol, bcol, st);
}

void voxieBox::PrintCtrl(int x, int y)
{
	Debug(x, y, 0xffc080, -1, "CTRL: Left (%d)  Right (%d)", voxie_keystat(K_Control_Left), voxie_keystat(K_Control_Right));
}

voxieBox & voxieBox::Instance()
{
	static voxieBox instance; // Guaranteed to be destroyed.
							  // Instantiated on first use.
	return instance;
}

void voxieBox::updateMouse()
{
	mouse.x += in.dmousx; mouse.y += in.dmousy; mouse.z += in.dmousz; 
}

void voxieBox::reportMouse(point3d position)
{
	point3d dd, rr, pp;

	

	pp.x = position.x;  rr.x = 0.04f; dd.x = 0.015f;
	pp.y = position.y;	rr.y = 0.0f; dd.y = 0.080f;
	pp.z = position.z; 	rr.z = 0.0f; dd.z = 0.0f;

	voxie_printalph_(&pp, &rr, &dd, 0xffffff, "Pos X%.0f, Y%.0f, Z%.0f Delta Move X%.0f, Y%.0f, Z%.0f", mouse.x, mouse.y, mouse.z, getMouseXDelta(), getMouseYDelta(), getMouseZDelta());
	pp.y -= -0.120f;

	voxie_printalph_(&pp, &rr, &dd, 0xffffff, "Buttons Raw: %d is Down: LC %d RC %d, MC %d ", getMouseButtonState(), getMouseClickIsDown(LEFT_BUTTON), getMouseClickIsDown(RIGHT_BUTTON), getMouseClickIsDown(MIDDLE_BUTTON));
	pp.y -= -0.120f;

	voxie_printalph_(&pp, &rr, &dd, 0xffffff, "Buttons On Down: LC %d RC %d, MC %d ", getMouseClickOnDown(LEFT_BUTTON), getMouseClickOnDown(RIGHT_BUTTON), getMouseClickOnDown(MIDDLE_BUTTON));
	pp.y -= -0.120f;

	voxie_printalph_(&pp, &rr, &dd, 0xffffff, "Buttons On Up: LC %d RC %d, MC %d ", getMouseClickOnUp(LEFT_BUTTON), getMouseClickOnUp(RIGHT_BUTTON), getMouseClickOnUp(MIDDLE_BUTTON));
	pp.y -= -0.120f;

	DrawSphere((mouse.x * .001f), (mouse.y * .001f), (mouse.z * .0004f), 0.05f, 1, 0xffffff);
}

float voxieBox::getMouseXDelta()
{
	return in.dmousx;
}

float voxieBox::getMouseYDelta()
{
	return in.dmousy;
}

float voxieBox::getMouseZDelta()
{
	return in.dmousz;
}

point3d voxieBox::getMousePosition()
{
	return mouse;
}

int voxieBox::getMouseButtonState()
{
	return in.bstat;
}

bool voxieBox::getMouseClickIsDown(MouseButton button)
{
	return ( ( in.bstat >> button) & 1);
}

bool voxieBox::getMouseClickOnDown(MouseButton button)
{
	return ((in.bstat >> button) & 1 ) && !((in.obstat >> button) & 1);
}

bool voxieBox::getMouseClickOnUp(MouseButton button)
{
	return (!(in.bstat >> button) & 1) && ((in.obstat >> button) & 1);
}

void voxieBox::DrawVox(float fx, float fy, float fz, int col)
{
	voxie_drawvox(&vf, fx, fy, fz, col);
}

void voxieBox::DrawBox(float x0, float y0, float z0, float x1, float y1, float z1, int fillmode, int colour)
{
	voxie_drawbox(&vf, x0, y0, z0, x1, y1, z1, fillmode, colour);
}

void voxieBox::DrawLine(float x0, float y0, float z0, float x1, float y1, float z1, int colour)
{
	voxie_drawlin(&vf, x0, y0, z0, x1, y1, z1, colour);
}

void voxieBox::DrawPolygon(pol_t * pt, int pt_count, int colour)
{
	voxie_drawpol(&vf, pt, pt_count, colour)
}

void voxieBox::DrawMeshTex(char * fnam, poltex_t * vt, int vtn, int * mesh, int meshn, int flags, int colour)
{
	voxie_drawmeshtex(&vf, fnam, vt, vtn, mesh, meshn, flags, colour);
}

void voxieBox::DrawCone(float x0, float y0, float z0, float r0, float x1, float y1, float z1, float r1, int fillmode, int colour)
{
	voxie_drawcone(&vf, x0, y0, z0, r0, x1, y1, z1, r1, fillmode, colour);
}

void voxieBox::DrawSprite(const char * fnam, point3d * p, point3d * r, point3d * d, point3d * f, int colour)
{
	voxie_drawspr(&vf, fnam, p, r, d, f, colour);
}

void voxieBox::PrintAlpha(point3d * p, point3d * r, point3d * d, int colour, const char * st)
{
	voxie_printalph(&vf, p, r, d, colour, st);
}

void voxieBox::DrawCube(point3d * p, point3d * r, point3d * d, point3d * f, int fillmode, int colour)
{
	voxie_drawcube(&vf, p, r, d, f, fillmode, colour);
}

void voxieBox::DrawHeightMap(char * fnam, point3d * p, point3d * r, point3d * d, point3d * f, int colorkey, int reserved, int flags)
{
	voxie_drawheimap(&vf, fnam, p, r, d, f, colorkey, reserved, flags);
}

void voxieBox::DrawSphere(float x, float y, float z, float radius, int fill, int col)
{
	voxie_drawsph(&vf, x, y, z, radius, fill,  col);
}